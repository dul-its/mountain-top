package main

import (
	"context"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"os/signal"
)

// Server strct to represent server
type Server struct {
	config *Config
	auth   *Auth

	// mux will manage the route-to-handler mappings
	handler http.Handler

	layoutBaseDir string
}

type layoutKeyName string

// NewServer will return a new server instance
func NewServer(config *Config, auth *Auth) *Server {
	server := &Server{}
	server.config = config
	server.auth = auth
	return server
}

// Handler will do some magical stuff, soon
func (s *Server) Handler() http.Handler {
	// mux.Handle("/config", s.config.NewConfigHTTPHandler())

	return s.handler
}

// SetHandler sets a route-to-handler mapping
func (s *Server) SetHandler(h http.Handler) {
	s.handler = h
}

// Run will run the server
func (s *Server) Run() {
	logHandler := LogMessageHandler(s.handler)

	httpServer := &http.Server{
		Addr:    ":" + s.config.Server.Port,
		Handler: logHandler,
	}

	go func() {
		var err error
		fmt.Printf("Running server on port %s. Happy Hiking...\n", s.config.Server.Port)
		fmt.Printf("Type [Ctrl-C] to quit\n")
		httpServer.ListenAndServe()
		if err != nil {
			fmt.Println(err)
		}
	}()
	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught
	signal.Notify(c, os.Interrupt)

	// Block until we recieve our signal
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), s.config.Server.GracefulWait)
	defer cancel()
	httpServer.Shutdown(ctx)
	fmt.Println("shutting down gracefully...")
}

// Helper functions
func (s *Server) layoutTemplate(layout string) *template.Template {
	if layout == "" {
		layout = "main"
	}
	filename := fmt.Sprintf("%s/%s.tmpl", s.layoutBaseDir, layout)
	layoutTemplate := template.Must(template.New("base").ParseFiles(filename))
	return layoutTemplate
}
