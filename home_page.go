package main

import (
	"fmt"
	"html/template"
	"net/http"
)

var cachedHomePageHandler *HomePageHandler = nil

// HomePageHandler will display status of active
type HomePageHandler struct {
	WebPageHandler

	config *Config

	// dstore provides access to the DB pointer
	dstore *DataStore
}

// HomePage is currently an arbitrary
// struct for testing
type HomePage struct {
	PageTitle     string
	StatusMessage string
}

// NewHomePageHandler returns a new Statu
func NewHomePageHandler(tpl *template.Template, c *Config, ds *DataStore) *HomePageHandler {
	if cachedHomePageHandler != nil {
		return cachedHomePageHandler
	}
	handler := &HomePageHandler{
		WebPageHandler: WebPageHandler{
			Tpl: tpl,
		},
		config: c, dstore: ds,
	}
	cachedHomePageHandler = handler
	return handler
}

// ServeHTTP allows us to identify HomePage as an http.Handler
func (sp *HomePageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, "Page Not Found (404)\n")
		return
	}
	sp.Template().ExecuteTemplate(w, "index.html", nil)
}
