package main

import (
	"flag"
	"fmt"
	"strings"
)

// CommandLineService uses "flag" to parse any and all flags
// provided on command line
type CommandLineService struct {
	ShowConfigOnly bool
	ShowDebugging  bool
	ConfigFilename string
	VerifyYAML     bool
	ServerMode     bool
}

// NewCommandLineService returns a new CommandLineService
// with command line flags parsed
func NewCommandLineService() *CommandLineService {
	cs := CommandLineService{}

	flag.BoolVar(&cs.ShowDebugging, "debug", false, "display debugging")
	flag.BoolVar(&cs.VerifyYAML, "verifyyaml", false, "verify config file YAML")
	flag.BoolVar(&cs.ServerMode, "server", true, "run in server/api mode")
	flag.BoolVar(&cs.ShowConfigOnly, "config-only", false, "display config and exit")
	flag.StringVar(&cs.ConfigFilename, "cfg", "config.yml", "config file location")

	flag.Parse()

	return &cs
}

// String prints a readable representation of the command-line args
func (cs *CommandLineService) String() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("ShowDebugging: %v\n", cs.ShowDebugging))
	sb.WriteString(fmt.Sprintf("VerifyYAML: %v\n", cs.VerifyYAML))
	sb.WriteString(fmt.Sprintf("ServerMode: %v\n", cs.ServerMode))
	sb.WriteString(fmt.Sprintf("ShowConfigOnly: %v\n", cs.ShowConfigOnly))

	return sb.String()
}
