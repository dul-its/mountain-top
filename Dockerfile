FROM image-mirror-prod-registry.cloud.duke.edu/library/golang:1.20-bookworm

# switch to the root user 
# to setup the image
USER 0

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

RUN apt-get update && apt-get install -y wait-for-it \
    && rm -rf /var/lib/apt/lists/*

# Create and then move to a directory named "build"
WORKDIR /build

# Copy and, then, download dependencies using 'go mod'
COPY go.mod .
RUN go mod tidy
COPY go.sum .
RUN go mod download

# Copy the code into the container
COPY . .

# Build the application
RUN go build -o main .

# Move the resulting binary to /dist
WORKDIR /dist
COPY config.yml .
COPY templates templates
COPY public public
RUN cp /build/main .

EXPOSE 4000

# Return back to the non-root user
USER 1001

# Command to run when starting the container
CMD ["/dist/main"]
