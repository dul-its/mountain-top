package main

import (
	"fmt"

	"github.com/redis/go-redis/v9"
)

// RedisService keeps track of a "Redis client"
type RedisService struct {
	config *Config
	Client *redis.Client
}

// NewRedisService ...
// Return a new Redis Client
func NewRedisService(c *Config) *RedisService {
	addr := fmt.Sprintf("%s:%s", c.Redis.Host, c.Redis.Port)
	return &RedisService{
		Client: redis.NewClient(&redis.Options{
			Addr: addr,
		}),
	}
}
