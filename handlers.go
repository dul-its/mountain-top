package main

import (
	"html/template"
	"log"
	"net/http"

	"github.com/felixge/httpsnoop"
)

// WebPageHandler is middleware that is intended to take the
// output of a secondary handler (StatusPageHandler, for instance)
// and use it as "data" context.
//
// From there, execute a "layoutTemplate" with that "data" context
//
// NOTE -- this is a candidate for a shared module package
type WebPageHandler struct {
	Tpl *template.Template
}

// Template returns the template
func (w WebPageHandler) Template() *template.Template {
	return w.Tpl
}

// LogMessageHandler will write simple log output
func LogMessageHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger, w := makeLogger(w)

		next.ServeHTTP(w, r)
		log.Printf("%s - \"%s %s %s\" %d %d", r.RemoteAddr, r.Method, r.URL.Path, r.Proto, logger.Status(), logger.Size())
	})
}

func makeLogger(w http.ResponseWriter) (*responseLogger, http.ResponseWriter) {
	logger := &responseLogger{w: w, status: http.StatusOK}
	return logger, httpsnoop.Wrap(w, httpsnoop.Hooks{
		Write: func(httpsnoop.WriteFunc) httpsnoop.WriteFunc {
			return logger.Write
		},
		WriteHeader: func(httpsnoop.WriteHeaderFunc) httpsnoop.WriteHeaderFunc {
			return logger.WriteHeader
		},
	})
}
