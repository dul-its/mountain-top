package main

import (
	"database/sql"
	"fmt"
)

// ConnectedDatabase returns a ready-to-use database,
// or an error as to why it didn't work out that way.
func ConnectedDatabase(config *Config) *sql.DB {
	var dsn string

	fmt.Println("ConnectedDatabase called...")

	switch config.Database.Driver {
	case "mysql":
		dsn = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
			config.Database.DbUser,
			config.Database.Password,
			config.Database.DbHost,
			config.Database.Port,
			config.Database.DbName,
		)
	case "postgres":
		dsn = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			config.Database.DbHost,
			config.Database.Port,
			config.Database.DbUser,
			config.Database.Password,
			config.Database.DbName,
		)
	}
	fmt.Printf("dataase dsn = [%s]\n", dsn)
	connectedDatabase, err := sql.Open(config.Database.Driver, dsn)
	if err != nil {
		panic(err)
	}
	return connectedDatabase
}
