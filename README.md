# Top Textbooks + Course Reserves Data

## Prerequisites

I believe that `.netrc` still needs to exist on the develop's workstion (`/home/<netID>/.netrc`).
 
## The `main.go` File
### Use of `dig` package for "dependency injection"
The project leans on dependency injection via the `dig` package, and uses:  
* `container.Provide(<someTypeUsedElsewhere>)`, and
* `container.Invoke(<someInvocation>)`
  
### Create Gitlab Personal Access Token
Create a Personal Access Token in Gitlab, then copy the token.

### Create `.netrc`
Create `/home/<netID>/.netrc`, then type/paste:
```bash
machine gitlab.oit.duke.edu
login git@gitlab.oit.duke.edu
password <your-token-here>
```
  
The purpose of this is to allow downloading of modules hosted on our Gitlab site.  

# Getting started

Clone the repository, using:  
  
```bash
$ git clone git@gitlab.oit.duke.edu:dul-its/mountain-top.git
$ cd /path/to/mountain-top
```
## Create `docker-compose.yml`
In order to run the stack, you'll need to create `docker-compose.yml`. Simply `cp docker-compose.example.yml docker-compose.yml`, 
then make desired adjustments.
  
## Create `config.yml`
The repository comes with a `config.example.yml` with most defaults provided. Please `cp config.example.yml config.yml`.  

## Run the Application
### Docker
```bash
$ docker-compose up --build
```

### Run Directly
If you have the latest version of `golang`, then run `go run .`

## Demo: Token Registration
With the application stack running (via `docker-compose up --build`), use the following command to simulate token registration:  
  
```bash
$ curl -X POST -H 'audience: "yes"' -H 'X-Requested-For: "XMLHttpRequest"' --data '{"username":"<your-netid-here>","password":"<pick-something>"}' localhost:4000/token/register
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImRsYzMyIiwiYXVkIjpbImR1bC1hdWRpZW5jZSJdLCJleHAiOjE2OTQ0NTM5MTgsImp0aSI6IjY3ZTEwNTc5LWY2YzUtNDhhNi05NDIxLTYyMTgwYjYxODFkZiJ9.SBJ6AiXFsitqzCT1tJhAAA-Asu5jt2O6uFvN6V1r8bE
```
  
Copy that long string representing the token. Then...

## Demo: Accessing a Protected Route with Token
```bash
$ curl -X POST --data '{"username":"dlc32","token":"eyJhbGciOiJIUzI1...."}' localhost:4000/protected
This is a protected route. Since we made it here, we're safe, right?
```
