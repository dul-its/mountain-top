package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"github.com/redis/go-redis/v9"
)

// Auth
type Auth struct {
	// Reference to the config that should be
	// initialized and setup at this point
	config *Config

	// Token service that we'll use to
	// generate and fetch tokens
	tokenService *TokenService

	// Access to redis client
	redis *RedisService
}

// NewAuth provides a new Auth instance
func NewAuth(c *Config, ts *TokenService, r *RedisService) *Auth {
	fmt.Println("initiating a new auth...")
	return &Auth{
		config:       c,
		tokenService: ts,
		redis:        r,
	}
}

// Credentials for username/password struct
type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
	Token    string `json:"token"`
}

// TokenRegisterHandler responds to token requests
func (a *Auth) TokenRegisterHandler(w http.ResponseWriter, r *http.Request) {
	// in the off case that we make it here
	// without having checked the request method
	if r.Method != http.MethodPost {
		return
	}
	// Log the event
	fmt.Printf("reached handler for /token/register route...\n")

	var (
		creds Credentials
	)

	// Get The JSON body, decoding into credentials
	err := json.NewDecoder(r.Body).Decode(&creds)

	expirationTime := time.Now().Add(30 * time.Minute)
	expiresAt := jwt.NewNumericDate(expirationTime)
	// Create a claims instance, initially with an
	// Expires date
	audience := r.Header.Get("audience")

	tokenString, _, err := a.tokenService.GeneratedToken(creds.Username, expiresAt, audience)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(err)
		return
	}

	// set the key
	cxt := context.Background()
	err = a.redis.Client.Set(cxt, fmt.Sprintf("token-%s", creds.Username), tokenString, 0).Err()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println(err)
		return
	}

	io.WriteString(w, fmt.Sprintf("%s\n", tokenString))
	fmt.Println("Successfully created token")
}

// RedisClient returns the, well, RedisClient :)
func (a *Auth) RedisClient() *redis.Client { return a.redis.Client }

// NewTokenAuthWrapper does fancy stuff
func (a *Auth) NewTokenAuthWrapper(handler func(http.ResponseWriter, *http.Request)) TokenAuthWrapper {
	return TokenAuthWrapper{
		auth:    a,
		handler: handler,
	}
}

// TokenAuthWrapper simply wraps an http.Handler with an auth instance.
type TokenAuthWrapper struct {
	handler func(http.ResponseWriter, *http.Request)

	// Auth service
	auth *Auth
}

func (aw TokenAuthWrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		var creds Credentials
		err := json.NewDecoder(r.Body).Decode(&creds)
		if err != nil {
			http.Error(w, "Unable to parse credentials in POST request", 500)
			return
		}

		validToken, err := aw.hasValidToken(creds)
		if err != nil {
			http.Error(w, "Unable to determine valid credentials", 500)
			return
		}

		if !validToken {
			http.Error(w, "Unauthorized request.", 403)
			return
		}
		aw.handler(w, r)
	}
}

func (aw TokenAuthWrapper) hasValidToken(creds Credentials) (bool, error) {
	redisClient := aw.auth.RedisClient()
	ctx := context.Background()
	if redisClient == nil {
		fmt.Println("redisClient is not located")
		return false, errors.New("Unable to reach Redis client")
	}

	tokenKey := fmt.Sprintf("token-%s", creds.Username)
	fmt.Printf("token key is [%s]\n", tokenKey)
	val, err := redisClient.Get(ctx, tokenKey).Result()

	if err != nil {
		fmt.Println("token doesn't exist...")
		return false, errors.New("token key does not exist")
	}

	return val == creds.Token, nil
}
