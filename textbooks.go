package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
	"time"
)

// Course represents a single course.
type Course struct {
	ID                  string        `json:"id"`
	Name                string        `json:"name"`
	CourseListingID     string        `json:"courseListingId"`
	CourseNumber        string        `json:"courseNumber"`
	CourseListingObject CourseListing `json:"courseListingObject"`
}

// CoursesResponse represents a list of courses
type CoursesResponse struct {
	Courses []Course `json:"courses"`
}

// CourseListing represents a course listing
type CourseListing struct {
	ID                string `json:"id"`
	ExternalID        string `json:"externalId"`
	TermID            string `json:"termId"`
	TermObject        Term   `json:"termObject"`
	InstructorObjects []struct {
		Name string `json:"name"`
	} `json:"instructorObjects"`
}

// Term represents a date range for a "term"
type Term struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
}

// CourseListingsResponse contains a list of CourseListings
type CourseListingsResponse struct {
	CourseListings []CourseListing `json:"courseListings"`
}

// AggregatedCourse is another type of course
type AggregatedCourse struct {
	ID            string   `json:"id"`
	Name          string   `json:"name"`
	CourseNumbers []string `json:"courseNumbers"`
	Instructors   []string `json:"instructors"`
	TermStartDate string   `json:"termStartDate"`
	TermEndDate   string   `json:"termEndDate"`
}

// Reserve represents a reserved "course"
type Reserve struct {
	ID                 string     `json:"id"`
	CourseListingID    string     `json:"courseListingId"`
	ProcessingStatusID string     `json:"processingStatusId"`
	StartDate          string     `json:"startDate"`
	EndDate            string     `json:"endDate"`
	CopiedItem         CopiedItem `json:"copiedItem"`
}

// CopiedItem is an item copied elsewhere
type CopiedItem struct {
	Title        string `json:"title"`
	Contributors []struct {
		Name string `json:"name"`
	} `json:"contributors"`
	InstanceHrid string `json:"instanceHrid"`
	Uri          string `json:"uri"`
}

// ReservesResponse ... more to come
type ReservesResponse struct {
	Reserves []Reserve `json:"reserves"`
}

// Textbook represents data for a course textbook
type Textbook struct {
	CourseCodes     string `json:"CourseCodes"`
	CourseName      string `json:"CourseName"`
	InstructorNames string `json:"InstructorNames"`
	ItemTitle       string `json:"ItemTitle"`
	Contributors    string `json:"Contributors"`
	BuildURL        string `json:"FindItem"`
}

// TextbookService manages a list of Textbooks
type TextbookService struct {
	config       *Config
	textbookFeed []Textbook
}

func (ts *TextbookService) apiCallURLs() (string, string, string) {
	queryCoursesReserves := "limit=10000&query=%28courseListing.courseTypeId%3D%3D%28%22" + ts.config.TextbookService.CourseTypes.Id1 + "%22%20or%20%22" + ts.config.TextbookService.CourseTypes.Id2 + "%22%20or%20%22" + ts.config.TextbookService.CourseTypes.Id3 + "%22%29%20and%20courseListing.termId%3D%3D%22" + ts.config.TextbookService.TermID + "%22%29%20sortby%20name"
	queryCourseListings := "limit=10000&query=%28courseTypeId%3D%3D%28%22" + ts.config.TextbookService.CourseTypes.Id1 + "%22%20or%20%22" + ts.config.TextbookService.CourseTypes.Id2 + "%22%20or%20%22" + ts.config.TextbookService.CourseTypes.Id3 + "%22%29%20and%20termId%3D%3D%22" + ts.config.TextbookService.TermID + "%22%29%20sortby%20name"
	urlCourses := fmt.Sprintf("%s/courses?%s", ts.config.TextbookService.BaseURL, queryCoursesReserves)
	urlCourseListings := fmt.Sprintf("%s/courselistings?%s", ts.config.TextbookService.BaseURL, queryCourseListings)
	urlReserves := fmt.Sprintf("%s/reserves?%s", ts.config.TextbookService.BaseURL, queryCoursesReserves)
	return urlCourses, urlCourseListings, urlReserves
}

func (ts *TextbookService) fetchAPIData(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("x-okapi-tenant", ts.config.TextbookService.Tenant)
	req.Header.Set("x-okapi-token", ts.config.TextbookService.APIToken)

	return http.DefaultClient.Do(req)
}

func (ts *TextbookService) fetchAndDecodeJSON(url string, v interface{}) error {
	resp, err := ts.fetchAPIData(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(v)
}

func (ts *TextbookService) fetchCourses(url string) (CoursesResponse, error) {
	var coursesResponse CoursesResponse
	err := ts.fetchAndDecodeJSON(url, &coursesResponse)
	return coursesResponse, err
}

func (ts *TextbookService) fetchCourseListings(url string) (CourseListingsResponse, error) {
	var courseListingsResponse CourseListingsResponse
	err := ts.fetchAndDecodeJSON(url, &courseListingsResponse)
	return courseListingsResponse, err
}

func (ts *TextbookService) fetchReserves(url string) (ReservesResponse, error) {
	var reservesResponse ReservesResponse
	err := ts.fetchAndDecodeJSON(url, &reservesResponse)
	return reservesResponse, err
}

// Helper function to join strings with a ";" separator.
func joinStrings(strs []string) string {
	return strings.Join(strs, "; ")
}

func uniqueStrings(strs []string) []string {
	set := make(map[string]bool)
	var uniqueList []string

	for _, s := range strs {
		if _, seen := set[s]; !seen {
			set[s] = true
			uniqueList = append(uniqueList, s)
		}
	}

	return uniqueList
}

// Convert the list of courses to a map for O(1) lookup.
func mapCoursesByListingID(coursesResponse CoursesResponse) map[string][]Course {
	coursesMap := make(map[string][]Course)
	for _, course := range coursesResponse.Courses {
		coursesMap[course.CourseListingID] = append(coursesMap[course.CourseListingID], course)
	}
	return coursesMap
}

// For each course listing, aggregate its corresponding course data.
func aggregateCourseData(courseListingsResponse CourseListingsResponse, coursesResponse CoursesResponse) map[string]AggregatedCourse {
	aggregatedCoursesMap := make(map[string]AggregatedCourse)
	coursesMap := mapCoursesByListingID(coursesResponse)

	for _, courseListing := range courseListingsResponse.CourseListings {
		if relatedCourses, found := coursesMap[courseListing.ID]; found {
			var courseNumbers []string
			var longestName string
			var termStartDate string
			var termEndDate string

			for _, course := range relatedCourses {
				courseNumbers = append(courseNumbers, course.CourseNumber)
				if len(course.Name) > len(longestName) {
					longestName = course.Name
				}
				// Update term start and end date from the embedded CourseListingObject in the Course
				termStartDate = course.CourseListingObject.TermObject.StartDate
				termEndDate = course.CourseListingObject.TermObject.EndDate
			}

			// Ensuring course numbers are unique and then sorting them.
			courseNumbers = uniqueStrings(courseNumbers)
			sort.Strings(courseNumbers)

			var instructorNames []string
			for _, instructor := range courseListing.InstructorObjects {
				instructorNames = append(instructorNames, instructor.Name)
			}

			aggregatedCourse := AggregatedCourse{
				ID:            courseListing.ID,
				Name:          longestName,
				CourseNumbers: courseNumbers,
				Instructors:   instructorNames,
				TermStartDate: termStartDate,
				TermEndDate:   termEndDate,
			}
			aggregatedCoursesMap[courseListing.ID] = aggregatedCourse
		}
	}

	return aggregatedCoursesMap
}

func isReserveActive(reserveStartDate, reserveEndDate, termStartDate, termEndDate string) bool {
	// Get the current time
	currentTime := time.Now()

	// Use term dates if reserve's dates are empty
	if reserveStartDate == "" {
		reserveStartDate = termStartDate
	}
	if reserveEndDate == "" {
		reserveEndDate = termEndDate
	}

	// Parse the reserve start and end dates using parseDate
	start, err1 := parseDate(reserveStartDate)
	end, err2 := parseDate(reserveEndDate)

	// If there's an error in parsing any of the dates, consider the item as inactive
	if err1 != nil || err2 != nil {
		fmt.Printf("Error parsing reserve dates: %v, %v\n", err1, err2)
		return false
	}

	// Subtract 7 days from the start date
	start = start.Add(-24 * time.Hour * 7)

	// Check if current time is within the start and end date range
	return currentTime.After(start) && currentTime.Before(end)
}

func parseDate(dateStr string) (time.Time, error) {
	// Try parsing without milliseconds
	t, err := time.Parse(time.RFC3339, dateStr)
	if err == nil {
		return t, nil
	}

	// If parsing without milliseconds failed, try with milliseconds
	t, err = time.Parse("2006-01-02T15:04:05.000Z", dateStr)
	if err == nil {
		return t, nil
	}

	return time.Time{}, err
}

func (ts *TextbookService) processReserves(reserves []Reserve, aggregatedCourses map[string]AggregatedCourse) []Textbook {
	seen := make(map[Textbook]bool)
	var textbooks []Textbook

	for _, reserve := range reserves {
		if course, exists := aggregatedCourses[reserve.CourseListingID]; exists {
			if !isReserveActive(reserve.StartDate, reserve.EndDate, course.TermStartDate, course.TermEndDate) {
				continue
			}
			dukeID := "DUKE" + reserve.CopiedItem.InstanceHrid[4:]
			titleParts := strings.Split(reserve.CopiedItem.Title, " / ")
			title := titleParts[0]

			var contributors []string
			for _, contributor := range reserve.CopiedItem.Contributors {
				contributors = append(contributors, contributor.Name)
			}

			urlText := ts.config.TextbookService.FindItem.DefaultURLText
			url := ts.config.TextbookService.FindItem.DefaultURL

			if reserve.CopiedItem.Uri == "" && (reserve.ProcessingStatusID == ts.config.TextbookService.ReadyStatus || reserve.ProcessingStatusID == ts.config.TextbookService.ReadyNoDisplayLinkStatus) {
				url = "https://find.library.duke.edu/catalog/" + dukeID
				urlText = "See item info"
			} else if reserve.CopiedItem.Uri != "" && reserve.ProcessingStatusID == ts.config.TextbookService.ReadyStatus {
				url = reserve.CopiedItem.Uri
				urlText = "See item info"
			}

			textbook := Textbook{
				CourseCodes:     joinStrings(course.CourseNumbers),
				CourseName:      course.Name,
				InstructorNames: joinStrings(course.Instructors),
				ItemTitle:       title,
				Contributors:    joinStrings(contributors),
				BuildURL:        urlText + "|" + url,
			}
			if _, exists := seen[textbook]; !exists {
				textbooks = append(textbooks, textbook)
				seen[textbook] = true
			}
		}
	}

	return textbooks
}

// GenerateTextbooksFeed is responsible for fetching data
// and storing it into the app's MariaDB database.
func (ts *TextbookService) GenerateTextbooksFeed(db *sql.DB) {
	urlCourses, urlCourseListings, urlReserves := ts.apiCallURLs()

	coursesResponse, err := ts.fetchCourses(urlCourses)
	if err != nil {
		log.Printf("Error fetching courses: %v", err)
		return
	}

	courseListingsResponse, err := ts.fetchCourseListings(urlCourseListings)
	if err != nil {
		log.Printf("Error fetching course listings: %v", err)
		return
	}

	reservesResponse, err := ts.fetchReserves(urlReserves)
	if err != nil {
		log.Printf("Error fetching reserves: %v", err)
		return
	}

	aggregatedCourses := aggregateCourseData(courseListingsResponse, coursesResponse)

	textbookFeed := ts.processReserves(reservesResponse.Reserves, aggregatedCourses)

	//store textbookFeed in TextbookService instance
	ts.textbookFeed = textbookFeed

	// Check if the reserves table exists
	var count int
		queryCheckTable := fmt.Sprintf("SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '%s' AND table_name = 'reserves'", ts.config.Database.DbName)
	err = db.QueryRow(queryCheckTable).Scan(&count)
	if err != nil {
		log.Printf("Error checking for 'reserves' table: %v", err)
		return
	}

	if count == 0 {
		// Table does not exist, handle accordingly
		log.Printf("Error: Table 'reserves' does not exist in the database.")
		return
	}

	// Clear existing data in the "reserves" table
	_, err = db.Exec("DELETE FROM reserves")
	if err != nil {
		log.Printf("Error clearing reserves table: %v", err)
		return
	}

	// Prepare the SQL statement for inserting textbooks
	stmt, err := db.Prepare("INSERT INTO reserves (course_code, course_name, instructor_names, item_title, contributors, build_url, created) VALUES (?, ?, ?, ?, ?, ?, NOW())")
	if err != nil {
		log.Printf("Error preparing statement: %v", err)
		return
	}
	defer stmt.Close()

	// Iterate over the textbooks and insert them into the database
	for _, textbook := range textbookFeed {
		_, err := stmt.Exec(textbook.CourseCodes, textbook.CourseName, textbook.InstructorNames, textbook.ItemTitle, textbook.Contributors, textbook.BuildURL)
		if err != nil {
			log.Printf("Error inserting textbook into database: %v", err)
		}
	}

	fmt.Printf("Total number of records inserted: %d\n", len(textbookFeed))

}

// GenerateJSONFeed fetches all textbooks from the database
func (ts *TextbookService) GenerateJSONFeed(db *sql.DB) {
	// Prepare the SQL query to fetch all records from the reserves table
	query := `SELECT course_code, course_name, instructor_names, item_title, contributors, build_url FROM reserves`

	// Execute the query
	rows, err := db.Query(query)
	if err != nil {
		log.Fatalf("Error fetching textbooks from database: %v", err)
	}
	defer rows.Close()

	// Temporary slice to store fetched textbooks
	var textbooks []Textbook

	// Iterate through the result set
	for rows.Next() {
		var textbook Textbook
		// Scan each row into a Textbook struct
		err := rows.Scan(&textbook.CourseCodes, &textbook.CourseName, &textbook.InstructorNames, &textbook.ItemTitle, &textbook.Contributors, &textbook.BuildURL)
		if err != nil {
			log.Fatalf("Error scanning textbook row: %v", err)
		}
		// Append the textbook to the slice
		textbooks = append(textbooks, textbook)
	}

	// Check for errors from iterating over rows
	if err = rows.Err(); err != nil {
		log.Fatalf("Error iterating over textbook rows: %v", err)
	}

	// Store the fetched textbooks in the TextbookService instance
	ts.textbookFeed = textbooks

	fmt.Printf("Fetched %d textbooks from the database\n", len(textbooks))
}

// NewTextbookService returns a new TextbookService instance
func NewTextbookService(c *Config) *TextbookService {
	return &TextbookService{
		config: c,
	}
}

func GenerateTextbooksFeedHandler(ts *TextbookService, datastore *DataStore) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		// Call the method to generate and store the feed, passing the db connection
		ts.GenerateTextbooksFeed(datastore.DB)
		fmt.Fprintln(w, "Textbooks feed generated successfully")
	}
}
