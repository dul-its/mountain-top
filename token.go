package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

// TokenService manages token creation, retrieval
type TokenService struct {
	config *Config

	secretKeyBase []byte
}

// Claims is a struct that will be encoded to a JWT
type Claims struct {
	Username string `json:"username"`
	jwt.RegisteredClaims
}

// NewTokenService returns a service that generates tokens
func NewTokenService(c *Config) *TokenService {
	verifySecretKeyBase(c)
	return &TokenService{
		config: c,
	}
}

func verifySecretKeyBase(c *Config) {
	// Generate a secret key if one is not already provided.
	if c.Services.Tokens.SecretKeyBase == "" {
		fmt.Println("'secret_key_base' not present. Generating one from scratch")
		var Rando = rand.Reader
		b := make([]byte, 128)
		_, _ = Rando.Read(b)
		c.Services.Tokens.SecretKeyBase = base64.RawURLEncoding.EncodeToString(b)
	}
}

// GeneratedToken receives a username, timestamp, and
// an optional "audience",
// Stores generated token in keystore (Redis) and
// returns token to caller.
func (s *TokenService) GeneratedToken(username string,
	expiresAt *jwt.NumericDate,
	audience string) (token, uid string, err error) {
	s.secretKeyBase = []byte(s.config.Services.Tokens.SecretKeyBase)

	uid = uuid.New().String()
	registeredClaims := jwt.RegisteredClaims{
		ExpiresAt: expiresAt,
		ID:        uid,
	}
	if audience != "" {
		registeredClaims.Audience = jwt.ClaimStrings{"dul-audience"}
	}
	claims := &Claims{
		Username:         username,
		RegisteredClaims: registeredClaims,
	}

	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// create the token string
	token, err = jwtToken.SignedString(s.secretKeyBase)

	return
}
