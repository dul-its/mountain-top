package main

import "database/sql"

// DataStore simply keeps our database and Redis client in a
// convenient location
type DataStore struct {
	DB    *sql.DB
	Redis *RedisService
}

// NewDataStore returns a new DataStore
func NewDataStore(db *sql.DB, r *RedisService) *DataStore {
	return &DataStore{
		DB:    db,
		Redis: r,
	}
}
