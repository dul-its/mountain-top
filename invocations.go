package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

// WaitForPorts will wait for ports to be available
func WaitForPorts(config *Config) error {
	ports := []string{}

	// Wait for database host:port if available
	if config.Database.Port != 0 {
		ports = append(ports, fmt.Sprintf("%s:%d", config.Database.DbHost, config.Database.Port))
	}

	// Wait for Redis host:port
	ports = append(ports, fmt.Sprintf("%s:%s", config.Redis.Host, config.Redis.Port))
	fmt.Println("Waiting for services to come online...")

	waitErr := WaitForServices(ports, 30*time.Second)
	if waitErr != nil {
		fmt.Println("unable to establish connection(s) to DB and/or Redis...\n")
		os.Exit(1)
	} else {
		fmt.Println("data services are available. Let's proceed...")
	}
	fmt.Println("done waiting...")

	return nil
}

// PingRedis will simply ping the redis server
func PingRedis(r *RedisService) error {
	ctx := context.Background()
	result, pingErr := r.Client.Ping(ctx).Result()
	fmt.Printf("%s\n", result)
	return pingErr
}

// VerifyYAML simply status that the YAML file representing the
// app's configuration is OK, then exit.
//
// Use case -- we call the app with the "-verify" switch
// during the test stage of a CI pipeline run (GitLab)
func VerifyYAML(config *Config) {
	if config.VerifyYAML == true {
		fmt.Println("The YAML-based config file is OK.")
		os.Exit(0)
	}
}

// RunTheMainStuff does this and that
func RunTheMainStuff(ts *TextbookService,
	server *Server,
	auth *Auth,
	config *Config,
	datastore *DataStore,
	tmplService *TemplatesService) {

	if config.ServerMode == true {
		fmt.Printf("running in 'servermode'...\n")
		log.Println("preparing templates...")

		tErr := tmplService.PrepareTemplates()
		if tErr != nil {
			log.Fatalln(tErr)
		}

		log.Println("registering routes...")
		mux := http.NewServeMux()
		mux.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("public/assets"))))
		mux.HandleFunc("/token/register", auth.TokenRegisterHandler)

		mux.Handle("/protected", auth.NewTokenAuthWrapper(func(w http.ResponseWriter, r *http.Request) {
			io.WriteString(w, "This is a protected route. Since we made it here, we're safe, right?")
		}))

		mux.Handle("/generate-textbooks-feed", GenerateTextbooksFeedHandler(ts, datastore))
		mux.Handle("/textbooks", NewTextbookHTTPWrapper(ts, datastore))

		mux.Handle("/status/", NewStatusPageHandler(tmplService.Tpl, config, datastore))
		// ..but don't forget the non-slash route
		mux.Handle("/status", NewStatusPageHandler(tmplService.Tpl, config, datastore))

		mux.Handle("/", NewHomePageHandler(tmplService.Tpl, config, datastore))

		// Run() will run in a channel-based loop
		server.SetHandler(mux)
		server.Run()

		// Exit with no errors
		os.Exit(0)
	} else {
		// Generate Feeds one time, then exit(0)
	}
}
