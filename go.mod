module gitlab.oit.duke.edu/dul-its/mountain-top

go 1.15

require (
	github.com/felixge/httpsnoop v1.0.4
	github.com/go-sql-driver/mysql v1.7.1
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/google/uuid v1.3.1
	github.com/joho/godotenv v1.5.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.10.9
	github.com/redis/go-redis/v9 v9.1.0
	gitlab.oit.duke.edu/dul-go/journal v0.0.0-20190219194120-2a5b5450b878
	go.uber.org/dig v1.17.0
	gopkg.in/yaml.v2 v2.4.0
)
