package main

import (
	"database/sql"
	"encoding/json"
	"net/http"
)

// TextbooksHTTPWrapper associates a TextbookService with a
// database instance.
type TextbooksHTTPWrapper struct {
	textbookService *TextbookService
	db              *sql.DB
}

// NewTextbookHTTPWrapper creates an http wrapper and calls methods on the textbookService instance
func NewTextbookHTTPWrapper(ts *TextbookService, datastore *DataStore) *TextbooksHTTPWrapper {
	return &TextbooksHTTPWrapper{
		textbookService: ts,
		db:              datastore.DB,
	}
}

func (tw *TextbooksHTTPWrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Pass the database connection to the GenerateJSONFeed method
	tw.textbookService.GenerateJSONFeed(tw.db)
	json.NewEncoder(w).Encode(tw.textbookService.textbookFeed)
}
