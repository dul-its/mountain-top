DROP TABLE IF EXISTS `reserves`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_code` VARCHAR(50) NOT NULL DEFAULT '',
  `course_name` VARCHAR(150) NOT NULL DEFAULT '',
  `instructor_names` VARCHAR(255),
  `item_title` VARCHAR(500),
  `contributors` VARCHAR(500),
  `build_url` VARCHAR(200),
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reserves_course_code` (`course_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

