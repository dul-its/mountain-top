# OKD Setup for Discovery Catalog

## Checklist
**Create Secrets for Database and Secret Keys**  
  
```bash
$ kubectl create secret generic app-postgres --from-literal=postgres-password=$(openssl rand -base64 14) --from-literal=password=$(openssl rand -base64 14)

$ kubectl create secret generic app-secrets \
--from-literal=worldcat-apikey=<worldcatapikey> \
--from-literal=secret-key-base=<secret-key-base>
```
  
