package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

// Config represents the configuration of the
// Mountain-Top application
type Config struct {
	Debug bool

	// Config filename
	ConfigFileName string

	// Server (port and other settings)
	Server struct {
		Port         string        `yaml:"port"`
		Host         string        `yaml:"host"`
		GracefulWait time.Duration `yaml:"gracefulTimeout"`
	} `yaml:"server"`

	Redis struct {
		Port string `yaml:"port"`
		Host string `yaml:"host" envconfig:"MT_REDIS_HOST"`
	} `yaml:"redis"`

	Services struct {
		Tokens struct {
			DefaultAudience string `yaml:"audience"`
			// Secret Key Base
			SecretKeyBase string `yaml:"secret_key_base"`
		}
		Templates struct {
			BaseDir   string `yaml:"base_dir"`
			Extension string `yaml:"extension"`
		}
	} `yaml:"services"`

	Database struct {
		Driver   string `yaml:"driver"`
		Path     string `yaml:"path"`
		Port     int    `yaml:"port"`
		DbHost   string `yaml:"host"`
		DbName   string `yaml:"dbname"`
		DbUser   string `yaml:"dbuser" envconfig:"MT_DBUSER"`
		Password string `yaml:"password" envconfig:"MT_DBPASS"`
	} `yaml:"database"`

	// TextbookService configuration "global" variables here
	TextbookService struct {
		BaseURL                  string `yaml:"base_url"`
		Tenant                   string `yaml:"tenant"`
		ReadyStatus              string `yaml:"ready_status"`
		ReadyNoDisplayLinkStatus string `yaml:"ready_no_display_link_status"`
		APIToken                 string `yaml:"api_token"`
		CourseTypes              struct {
			Id1 string `yaml:"id1"`
			Id2 string `yaml:"id2"`
			Id3 string `yaml:"id3"`
		} `yaml:"course_types"`
		TermID   string `yaml:"term_id"`
		FindItem struct {
			DefaultURL     string `yaml:"default_url"`
			DefaultURLText string `yaml:"default_url_text"`
		} `yaml:"find_item"`
	} `yaml:"textbook_service"`

	VerifyYAML bool

	ServerMode bool

	// NOTES -- not used for anything
	// postgres
	// host=%s port=%d user=%s password=%s dbname=%s sslmode=disable"
	// .
	// mysql
	// user:password@tcp(dbhost:port)/dbname
}

// NewConfig returns a new Config, representing the
// application's configuraiton.
// Called by container.Provide in main.go
func NewConfig(cs *CommandLineService) *Config {
	fmt.Println("Initializing config...")

	fmt.Println("Let's check cli service...")
	fmt.Printf("%+v\n", cs)

	c := &Config{}
	c.parseConfigSources(cs.ConfigFilename)
	c.applyCommandLineFlags(cs)

	fmt.Printf("%+v\n", c)
	fmt.Printf("ServerMode = %t\n", c.ServerMode)

	return c
}

func (c *Config) applyCommandLineFlags(cs *CommandLineService) {
	c.Debug = c.Debug || cs.ShowDebugging
	c.VerifyYAML = cs.VerifyYAML
	c.ServerMode = cs.ServerMode
}

// ParseConfigSources parses config file
func (c *Config) parseConfigSources(filename string) {
	fmt.Println("Reading config sources (file + env)...")
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(c)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	// now parse the environment
	err = envconfig.Process("", c)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
}

// ToString - print a stringy-fied representation of
// our config.
func (c *Config) String() string {
	var s strings.Builder
	s.WriteString("Server:\n")
	s.WriteString(fmt.Sprintf("--Port: %s\n", c.Server.Port))
	s.WriteString(fmt.Sprintf("--Host: %s\n", c.Server.Host))
	s.WriteString(fmt.Sprintf("--GracefulWait: %s\n", c.Server.GracefulWait))
	s.WriteString("\n")
	s.WriteString("Database:\n")
	s.WriteString(fmt.Sprintf("--Driver: %s\n", c.Database.Driver))
	s.WriteString(fmt.Sprintf("--Path: %s\n", c.Database.Path))
	s.WriteString(fmt.Sprintf("--Port: %d\n", c.Database.Port))
	s.WriteString(fmt.Sprintf("--DbHost: %s\n", c.Database.DbHost))
	s.WriteString(fmt.Sprintf("--DbName: %s\n", c.Database.DbName))
	s.WriteString(fmt.Sprintf("--DbUser: %s\n", c.Database.DbUser))
	s.WriteString("--Password: <redacted>\n")
	s.WriteString("\n")
	s.WriteString("Redis:\n")
	s.WriteString(fmt.Sprintf("--Port: %s\n", c.Redis.Port))
	s.WriteString(fmt.Sprintf("--Host: %s\n", c.Redis.Host))
	return s.String()
}

// NewConfigHTTPHandler returns a new http handler
func (c *Config) NewConfigHTTPHandler() ConfigHTTPHandler {
	return ConfigHTTPHandler{
		config: c,
	}
}

// ConfigHTTPHandler will return a pretty-fied string dump
// of the current configuration.
//
// Using this merely as an example of how to create HttpHandler
// instances.
type ConfigHTTPHandler struct {
	config *Config
}

func (h ConfigHTTPHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, fmt.Sprintf("%s\n", h.config))
}
