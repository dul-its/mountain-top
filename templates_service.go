package main

import (
	"fmt"
	"html/template"
)

// TemplatesService manages the collection of templates (HTML?)
type TemplatesService struct {
	Tpl *template.Template

	config *Config
}

// NewTemplatesService returns a new TemplatesService
func NewTemplatesService(c *Config) *TemplatesService {
	return &TemplatesService{config: c}
}

// PrepareTemplates parses our template location
func (ts *TemplatesService) PrepareTemplates() error {
	baseDir := fmt.Sprintf("./%s/*.%s",
		ts.config.Services.Templates.BaseDir,
		ts.config.Services.Templates.Extension,
	)

	var err error
	ts.Tpl, err = template.ParseGlob(baseDir)
	if err != nil {
		return err
	}

	return nil
}
