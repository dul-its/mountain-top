package main

import (
	"fmt"
	"io"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	"go.uber.org/dig"
)

// BuildContainer builds all startup dependencies
func BuildContainer() *dig.Container {
	container := dig.New()

	container.Provide(NewCommandLineService)
	container.Provide(NewConfig)
	container.Provide(ConnectedDatabase)
	container.Provide(NewTextbookService)
	container.Provide(NewTokenService)
	container.Provide(NewRedisService)
	container.Provide(NewDataStore)
	container.Provide(NewAuth)
	container.Provide(NewServer)
	container.Provide(NewTemplatesService)

	return container
}

func doExport(w http.ResponseWriter, r *http.Request) {
	// Don't proceed unless this is a POST request
	if r.Method != http.MethodGet {
		return
	}
	// Send data to the assigned target data source
	// (redis, elastic, solr)

	// Tell everyone that all is well
	fmt.Printf("reached handler for /export route...\n")
	io.WriteString(w, "We would have sent data to a remote locale\n")

}

func getRoot(w http.ResponseWriter, r *http.Request) {
	// Log the event
	fmt.Printf("reached handler for / route...\n")

	// Send "Hello World :)" string to the responseWriter
	// instance
	io.WriteString(w, "Hello World :)\n")
}

func main() {
	// Build our dependency container
	container := BuildContainer()

	// NOTE the functions referred to here are located in invocations.go
	err := container.Invoke(WaitForPorts)
	if err != nil {
		panic(err)
	}
	err = container.Invoke(PingRedis)
	err = container.Invoke(VerifyYAML)
	err = container.Invoke(RunTheMainStuff)

	if err != nil {
		panic(err)
	}
}
