package main

import (
	"html/template"
	"log"
	"net/http"
)

var cachedStatusHandler *StatusPageHandler = nil

// StatusPageHandler will display status of active
type StatusPageHandler struct {
	WebPageHandler

	config *Config

	// dstore provides access to the DB pointer
	dstore *DataStore
}

// StatusPage is currently an arbitrary
// struct for testing
type StatusPage struct {
	PageTitle     string
	StatusMessage string
}

// NewStatusPageHandler returns a new Statu
func NewStatusPageHandler(tpl *template.Template, c *Config, ds *DataStore) *StatusPageHandler {
	if cachedStatusHandler != nil {
		return cachedStatusHandler
	}
	handler := &StatusPageHandler{
		WebPageHandler: WebPageHandler{
			Tpl: tpl,
		},
		config: c, dstore: ds,
	}
	cachedStatusHandler = handler
	return handler
}

// ServeHTTP allows us to identify StatusPage as an http.Handler
func (sp *StatusPageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("request path from statushandler is [%s]\n", r.URL.Path)

	// NOTE - this is where you'll get your data, whether it means
	// fetching it from a model service, or as in the case below,
	// mocking up a simple data struct.
	statusPageData := &StatusPage{
		PageTitle:     "Hello World",
		StatusMessage: "This status message is brought to you by Duke University Libraries!",
	}
	sp.Template().ExecuteTemplate(w, "status.html", statusPageData)
}
